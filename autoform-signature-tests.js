// Import Tinytest from the tinytest Meteor package.
import { Tinytest } from "meteor/tinytest";

// Import and rename a variable exported by autoform-signature.js.
import { name as packageName } from "meteor/perfectsofttunisia:autoform-signature";

// Write your tests here!
// Here is an example.
Tinytest.add('autoform-signature - example', function (test) {
  test.equal(packageName, "autoform-signature");
});
