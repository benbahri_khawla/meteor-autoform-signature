Package.describe({
  name: 'perfectsofttunisia:autoform-signature',
  version: '1.0.0',
  summary: 'upload signature',
  git: 'https://gitlab.com/benbahri_khawla/meteor-autoform-signature',
  documentation: 'README.md'
});


Package.onUse(function(api) {
  api.versionsFrom('1.2');

  api.use(['ecmascript', 'templating', 'reactive-var'], 'client');

  api.use('aldeed:autoform@4.0.0 || 5.0.0 || 6.0.0', {weak: true});
  api.use('perfectsofttunisia:autoform@4.0.0 || 5.0.0 || 6.0.0', {weak: true});

  api.addFiles([
    'autoform-signature-input.html',
    'autoform-signature-input.js',
    'input-type-config.js',
  ], 'client');

});
Package.onTest(function(api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('perfectsofttunisia:autoform-signature');
  api.mainModule('autoform-signature-tests.js');
});



