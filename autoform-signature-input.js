import SignaturePad from 'signature_pad';

let wrapper,
    canvas,
    signaturePad;

Template.afSignature.onRendered(function () {
    wrapper = document.getElementById('signature-pad');
    canvas = wrapper.querySelector('canvas');
    function resizeCanvas() {
        let ratio =  Math.max(window.devicePixelRatio || 1, 1);
        canvas.width = canvas.offsetWidth * ratio;
        canvas.height = canvas.offsetHeight * ratio;
        canvas.getContext('2d').scale(ratio, ratio);
    }
  
    window.onresize = resizeCanvas;
    resizeCanvas();
    
    signaturePad = new SignaturePad(canvas);
});

Template.afSignature.onCreated(function () {
    this.dataURI = new ReactiveVar(false);
});

Template.afSignature.helpers({
    schemaKey() {
        if (this.atts) {
            return this.atts['data-schema-key'];
        }
    },
    dataURI() {
        return Template.instance().dataURI.get();
    }
});

Template.afSignature.events({
    'click [data-action=clear]'() {
        event.preventDefault();
        signaturePad.clear();
    },
    'click [data-action=save]'(event, instance) {
        event.preventDefault();
        let file = signaturePad.toDataURL();
        if (file) {
            Meteor.call('Images.insert', {file}, (error, result) => {
                if (!error) {
                    instance.dataURI.set(result);
                }
            });
        }
    },
});